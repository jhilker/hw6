import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.nonparametric.smoothers_lowess import lowess
import sqlite3


conn = sqlite3.connect('Data/FPA_FOD_20170508.sqlite')

fires = pd.read_sql("select * from Fires limit 1000", conn)

## Percentages of Fires by Size
fire_tab = pd.crosstab(fires.FIRE_SIZE_CLASS,fires.FIRE_YEAR,margins=True)
print(fire_tab / fire_tab.loc['All']['All'] * 100)


